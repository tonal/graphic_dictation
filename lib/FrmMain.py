# -*- codong: utf-8 -*-
from operator import attrgetter
from typing import Sequence

from PyQt5 import QtWidgets, QtCore, QtGui, uic

from lib import graph_dict_proc
from lib import ui_FrmMain
from lib.graph_dict_proc import cmd2bbox, cmd2coords
from lib.qtools import BlokSignal


class FrmMain(QtWidgets.QDialog):
  __ui = None
  __title:str = None
  __cmds:Sequence[str] = None
  __abs_coords = None
  __index:int = None
  __scene:QtWidgets.QGraphicsScene = None
  __cur_dot:QtWidgets.QGraphicsItem = None
  __voice: graph_dict_proc.Voice = None

  _color_fig:QtGui.QColor = QtGui.QColor('green')
  _color_beg:QtGui.QColor = QtGui.QColor('red')
  _color_cur:QtGui.QColor = QtGui.QColor('blue')

  def __init__(self):
    super().__init__()
    # UiFrmMain, _ = uic.loadUiType('data/ui/FrmMain.ui')
    self.__ui = ui = ui_FrmMain.Ui_FrmMain()
    ui.setupUi(self)
    self.__voice = graph_dict_proc.Voice()

  @QtCore.pyqtSlot()
  def on_btOpen_clicked(self):
    fname, _ = QtWidgets.QFileDialog.getOpenFileName(
      self, 'Выберите файл с диктантом', 'data/examples',
      'Диктант (*.txt);;Все (*.*)')
    if not fname:
      return
    title, cmds = graph_dict_proc.parse_file(fname)
    ui = self.__ui
    self.__title = title # = graph_dict_proc.SRC_TITLE
    self.__cmds = cmds # = graph_dict_proc.SCR
    self.__index = 0
    ui.lbTitle.setText(title)
    l_cmds = len(cmds)
    ui.lbCnt.setText(f'{l_cmds}')
    with BlokSignal(ui.sbCur):
      ui.sbCur.setValue(0)
    ui.sbCur.setMaximum(l_cmds)
    ui.sbCur.setEnabled(True)
    ui.btStart.setEnabled(True)
    ui.edText.clear()
    ui.lbArrow.clear()
    ui.lbCells.clear()
    self._paint_fig(cmds)

  def _paint_fig(self, cmds):
    ui = self.__ui
    self.__cur_dot = None
    if self.__scene:
      self.__scene.deleteLater()
    if self.__cur_dot:
      self.__cur_dot.deleteLater()
    scene = self.__scene = QtWidgets.QGraphicsScene(self)
    SCALE = 50
    abs_coords = self.__abs_coords = tuple(map(
      attrgetter('real', 'imag'), map(lambda p: p*SCALE, cmd2coords(cmds))))
    # min_x, min_y, max_x, max_y = cmd2bbox(cmds)
    min_x = min(x for x, _ in abs_coords)
    max_x = max(x for x, _ in abs_coords)
    min_y = min(y for _, y in abs_coords)
    max_y = max(y for _, y in abs_coords)
    rect = self.__rect = QtCore.QRectF(
      min_x - SCALE, min_y - SCALE,
      max_x - min_x + 2*SCALE, max_y - min_y + 2*SCALE)
    scene.addRect(rect)
    for x in range(int(min_x), int(max_x) + SCALE + 1, SCALE):
      scene.addLine(x, min_y - SCALE, x, max_y + SCALE)
    for y in range(int(min_y), int(max_y) + SCALE + 1, SCALE):
      scene.addLine(min_x - SCALE, y, max_x + SCALE, y)
    # pen = QtGui.QPen()
    # pen.setWidth(7)
    pen = QtGui.QPen(self._color_fig)
    pen.setWidth(5)
    for (s_x, s_y), (e_x, e_y) in zip(
      abs_coords, abs_coords[1:] + (abs_coords[0],)
    ):
      scene.addLine(s_x, s_y, e_x, e_y, pen)
    color = self._color_beg
    scene.addEllipse(
      QtCore.QRectF(-8, -8, 8, 8), pen=QtGui.QPen(color),
      brush=QtGui.QBrush(color))
    ui.graphicsView.resetTransform()
    ui.graphicsView.setScene(scene)
    ui.graphicsView.scale(1, -1)
    ui.graphicsView.fitInView(rect, QtCore.Qt.KeepAspectRatio)


  @QtCore.pyqtSlot()
  def on_btStart_clicked(self):
    ui = self.__ui
    with BlokSignal(ui.sbCur):
      ui.sbCur.setValue(0)
    self.__index = 0
    text = graph_dict_proc.scr2margin_text(self.__cmds)
    ui.edText.appendPlainText(text)
    if ui.cbSound.isChecked():
      self.__voice.sayText(text)

    ui.btNext.setEnabled(True)
    ui.lbArrow.clear()
    ui.lbCells.clear()

    # ui.graphicsView.resetTransform()
    # color = self._color_cur
    # x, y = self.__abs_coords[1]
    # scene = self.__scene
    # self.__cur_dot = scene.addEllipse(
    #   QtCore.QRectF(x - 8, y - 8, x + 8, y + 8), pen=QtGui.QPen(color),
    #   brush=QtGui.QBrush(color))
    # # ui.graphicsView.setScene(scene)
    # ui.graphicsView.scale(1, -1)
    # ui.graphicsView.fitInView(self.__rect, QtCore.Qt.KeepAspectRatio)
    # ui.graphicsView.update()

  @QtCore.pyqtSlot()
  def on_btNext_clicked(self):
    ind = self.__index
    src = self.__cmds
    cmd = src[ind]

    text = graph_dict_proc.cmd2text(cmd)

    ui = self.__ui
    ui.lbArrow.setText(cmd[-1])
    ui.lbCells.setText(cmd[:-1])
    ui.edText.appendPlainText(f'{ind + 1} {text}')
    if ui.cbSound.isChecked():
      self.__voice.sayText(text)
    ind += 1
    self.__index = ind
    if ind >= len(src):
      ui.btNext.setEnabled(False)
    else:
      with BlokSignal(ui.sbCur):
        ui.sbCur.setValue(ind + 1)
      # scene = self.__scene
      # scene.removeItem(self.__cur_dot)
      # color = self._color_cur
      # x, y = self.__abs_coords[ind] if ind < len(src) else self.__abs_coords[0]
      # self.__cur_dot = self.__scene.addEllipse(
      #   QtCore.QRectF(x - 8, y - 8, x + 8, y + 8), pen=QtGui.QPen(color),
      #   brush=QtGui.QBrush(color))
      # ui.graphicsView.update()

  @QtCore.pyqtSlot(int)
  def on_sbCur_valueChanged(self, ind:int):
    self.__index = ind - 1 if ind > 0 else 0
    cmds = self.__cmds
    if cmds and ind < len(cmds):
      self.__ui.btNext.setEnabled(True)
