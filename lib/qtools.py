# -*- codong: utf-8 -*-


class Enabler:
  """
  Запрещает работу с указанным виджетом в блоке with
  """
  def __init__(self, widget):
    self.widget = widget
  def __enter__(self):
    self.widget.setEnabled(False)
    return self.widget
  def __exit__(self, *exc_info):
    self.widget.setEnabled(True)


class UpdatesEnabler:
  """
  Запрещает обновление указанного виджета в блоке with
  """
  def __init__(self, widget):
    self.widget = widget
  def __enter__(self):
    self.widget.setUpdatesEnabled(False)
    return self.widget
  def __exit__(self, *exc_info):
    self.widget.setUpdatesEnabled(True)
    #self.widget.repaint()


class BlokSignal:
  """
  Блокирует сигналы указанного объекта в блоке with
  """
  def __init__(self, qtObj):
    self.qtObj = qtObj
  def __enter__(self):
    self.qtObj.blockSignals(True)
    return self.qtObj
  def __exit__(self, *exc_info):
    self.qtObj.blockSignals(False)

