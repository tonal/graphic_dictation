#! /usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Графический диктант
https://kopilpremudrosti.ru/risunki-po-kletochkam-v-tetradi.html
https://doshkolnik.net/podgotovka-k-shkole/graficheskie-diktanty/po-kletochkam-dlya-doshkolnikov.html
"""
from itertools import accumulate, chain
from operator import attrgetter
from pathlib import Path
import re
import subprocess

#import _festival
from typing import Union, Optional, Tuple

from num2words import num2words
from num2words.lang_RU import Num2Word_RU
import pymorphy2

SRC_TITLE = 'Ключик'

SCR = (
  #'1 клетка вправо', '1 клетка вверх', '1 клетка вправо', '1 клетка вниз',
  #'1 клетка вправо', '1 клетка вниз', '8 клеток вправо')
  # Отступить 1 клетку вправо и 2 клетки вниз
  '1п', '1в', '1п', '1н', '1п', '1н', '8п',
  '1в', '1п', '1в', '1п', '1н', '1п', '1н', '1п', '1н',
  '12л',
  '1н', '1л', '1н', '1л', '1в', '1л',
  '3в')


ARROWS = '←→↑↓'
RUSDIR = 'лпвн'
ENGDIR = 'lrud'


DIR_DICT = {a: a for a in ARROWS}
DIR_DICT.update({r: a for r, a in zip(RUSDIR, ARROWS)})
DIR_DICT.update({e: a for e, a in zip(ENGDIR, ARROWS)})

DIR2WORD = {'←': 'лево', '→': 'право', '↑': 'верх', '↓': 'низ'}
DIR2COORD = {'←': -1, '→': 1, '↑': 1j, '↓': -1j}

morph = pymorphy2.MorphAnalyzer()


def main():

  scr = SCR
  #subprocess.run(
  #  ['festival', '--tts', '--language', 'russian'], encoding='utf-8',
  #  input=', '.join(scr), )
  #fest = subprocess.Popen(
    #['festival', '--tts', '--pipe', '--language', 'russian'], encoding='utf-8',
    #stdin=subprocess.PIPE)
  voice = Voice()
  coord = 0
  for pf in scr:
    voice.sayText(cmd2text(pf))
    coord += cmd2coord(pf)
    print(coord)
    input()
  print(coord)


n2w = Num2Word_RU()


def scr2margin_text(cmds):
  abs_coords = cmd2coords(cmds)
  min_x = min(map(attrgetter('real'), abs_coords))
  max_y = max(map(attrgetter('imag'), abs_coords))
  marg_x = int(1 - min_x)
  marg_y = int(1 + max_y)

  cell = morph.parse('клетка')[0]
  cell_x = cell.make_agree_with_number(marg_x).word
  # cell_x = morph.parse(cell_x)[0].inflect({'sing', 'accs'}).word
  w_x = n2w._int2word(marg_x, feminine=True) #, to='ordinal')
  # w_x = morph.parse(w_x)[0].inflect({'sing', 'accs'}).word

  cell_y = cell.make_agree_with_number(marg_y).word
  # cell_y = morph.parse(cell_y)[0].inflect({'sing', 'accs'}).word
  w_y = n2w._int2word(marg_y, feminine=True) #, to='ordinal')
  # w_y = morph.parse(w_y)[0].inflect({'sing', 'accs'}).word

  marg_text = f'Отступите на {w_x} {cell_x} вправо и {w_y} {cell_y} вниз'
  return marg_text


def cmd2coords(cmds):
  rel_coords = map(cmd2coord, cmds)
  abs_coords = tuple(accumulate(rel_coords))
  return abs_coords


def cmd2bbox(cmds):
  abs_coords = cmd2coords(cmds)
  xs = tuple(map(attrgetter('real'), abs_coords))
  min_x = min(xs)
  max_x = max(xs)
  ys = tuple(map(attrgetter('imag'), abs_coords))
  min_y = min(ys)
  max_y = max(ys)
  return (min_x, min_y, max_x, max_y)


def cmd2text(cmd:str) -> str:
  cmd = cmd.strip().lower()

  direct = DIR2WORD[DIR_DICT[cmd[-1]]]
  cnt = int(cmd[:-1].rstrip())
  cell = morph.parse('клетка')[0]
  cells = cell.make_agree_with_number(cnt).word
  wcnt = n2w._int2word(cnt, feminine=True) #, to='ordinal')
  text = f'{wcnt} {cells} в{direct}'
  return text


def cmd2coord(cmd) -> complex:
  cmd = cmd.strip().lower()

  direct = DIR2COORD[DIR_DICT[cmd[-1]]]
  cnt = int(cmd[:-1].rstrip())
  step = cnt * direct
  return step


def parse_file(fname:Union[str, Path]) -> Optional[Tuple[str, Tuple[str, ...]]]:
  # return SRC_TITLE, SCR
  fname = Path(fname)
  title = fname.stem
  inp = fname.open()
  def strip_comm(li):
    li = li.split(' ;', 1)[0].strip()
    if not li or li.startswith(';'):
      return
    return li

  re_title = re.compile(
    r'(?:Диктант\s+) \W?(?P<title>\w?[\s\w]*\w)\W?', re.I | re.S | re.X
  ).match
  re_cmds = re.compile(
    r'(?P<cnt>\d+)\s* (?:клет\w*\s*)? в?(?P<dir>[←→↑↓нвплdulr])\w*',
    re.I | re.S | re.X)

  li_iter = filter(bool, map(strip_comm, inp))
  li_cmd = ''
  for li in li_iter:
    m = re_title(li)
    if m:
      title = m.group('title')
      break
    if re_cmds.search(li):
      li_cmd = li
      break

  if li_cmd:
    li_iter = chain([li_cmd], li_iter)

  fnd_cmd = re_cmds.finditer
  cmds = tuple(
    f'{m.group("cnt")}{DIR_DICT[m.group("dir")]}'
    for li in li_iter for m in fnd_cmd(li))
  # cmds = []
  # for li in li_iter:
  #   for m in fnd_cmd(li):
  #     cmds.append(f'{m.group("cnt")}{m.group("dir")}')
  return title, tuple(cmds)


class Voice:

  def __init__(self, lang='russian'):
    self.__lang = lang
    # https://pythono.ru/linux-rhvoice/
    # self.esng = ESpeakNG() https://github.com/gooofy/py-espeak-ng

  @property
  def lang(self):
    return self.__lang

  def sayText(self, text:str):
    print(text)
    subprocess.run(
      # ['festival', '--tts', '--language', self.lang], encoding='utf-8',
      ['RHVoice-test', '-p', 'anna'], encoding='utf-8',
      input=text,)


if __name__ == '__main__':
  main()