#! /usr/bin/env python3
# -*- coding: utf-8 -*-

import sys

from PyQt5 import QtGui, QtWidgets, QtCore

from lib import FrmMain


def _excepthook(type, value, tback):
  # log the exception here

  # then call the default handler
  sys.__excepthook__(type, value, tback)


sys.excepthook = _excepthook


def main():
  translator = QtCore.QTranslator()
  localename = QtCore.QLocale.system().name()
  translation = 'data/translations/graph_dict_' + localename
  translator.load(translation)

  app = QtWidgets.QApplication(sys.argv)
  app.setOrganizationName('Promsoft')
  app.setOrganizationDomain('promsoft.ru')
  app.setApplicationName('Graphic dictation')
  icon = QtGui.QIcon('data/icons/graph-dictation.png')
  app.setWindowIcon(icon)

  win = FrmMain.FrmMain() #None, 'Opa', 'Graphic dictation')
  win.show()
  return app.exec_()


if __name__ == '__main__':
  main()
